import requests
from bs4 import BeautifulSoup
from datetime import datetime
from dataclasses import dataclass


class RaceScraper:
    url: str

    def __init__(self, url: str):
        self.url = url

    def scrape_races(self) -> dict:
        # Scrape raw HTML data
        soup = self.make_soup()
        # Filter to useful race data
        event_name_list_in_html = self.filter_soup(soup, 'wedstrijden table normal', 'div')
        event_date_list_in_html = self.filter_soup(soup, 'wedstrijden table normal', 'time')

        # Format the data to a list
        event_name_list = self.format_names(event_name_list_in_html)
        event_date_list = self.format_dates(event_date_list_in_html)

        return self.make_event_dict(event_name_list, event_date_list)

    def make_soup(self):
        r = requests.get(self.url)
        return BeautifulSoup(r.content, 'lxml')

    # TODO: Add Type annotation for soup data type
    @staticmethod
    def filter_soup(soup, table_html_class: str, data_html_attribute: str) -> list:
        element_from_url = soup.find(class_=table_html_class)
        html_list = element_from_url.find_all(data_html_attribute)
        return html_list

    @staticmethod
    def format_names(event_name_list_in_html) -> list:
        list_of_items = [item.text[9:len(item) - 6] for item in event_name_list_in_html]
        return list_of_items[10::7]

    @staticmethod
    def format_dates(event_date_list_in_html) -> list:
        list_of_items = [item.text[17:len(item) - 27] for item in event_date_list_in_html]
        return [datetime.strptime(item, '%d %b %Y %H:%M') for item in list_of_items]

    @staticmethod
    def make_event_dict(event_name_list, event_date_list) -> dict:
        event_dict = dict()
        for (name, date) in zip(event_name_list, event_date_list):
            race_event_value = RaceEvent(name, date)
            race_event_key = datetime.strftime(date, '%d/%m/%Y')
            event_dict[race_event_key] = race_event_value
        return event_dict


@dataclass
class RaceEvent:
    name: str
    date: datetime

    def time_str(self) -> str:
        return datetime.strftime(self.date, '%H:%M')

    def date_str(self) -> str:
        return datetime.strftime(self.date, '%d/%m/%Y')
