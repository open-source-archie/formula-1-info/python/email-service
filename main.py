import os
import smtplib
import time
from datetime import datetime
import repository
import scraper
import f1_api


def get_current_time(strf_format: str) -> str:
    current_time = datetime.today()
    return datetime.strftime(current_time, strf_format)


def send_email(recipient_address: list,
               sender_address: str = os.getenv("SENDER_EMAIL_ADDRESS"),
               sender_password: str = os.getenv("SENDER_EMAIL_PASSWORD"),
               smtp_server: str = "smtp.gmail.com",
               tls_port: int = 587):

    subject = f"The {current_event.name} today starts at {current_event.time_str()}."
    body = ''

    with smtplib.SMTP(smtp_server, tls_port) as smtp:
        smtp.ehlo()
        smtp.starttls()
        smtp.ehlo()
        smtp.login(sender_address, sender_password)
        msg = f'Subject: {subject}\n\n{body}'
        smtp.sendmail(sender_address, recipient_address, msg)

    print(f'Email sent for the event {current_event.name} on {today_date_str} at {get_current_time("%H:%M:%S")}')


if __name__ == '__main__':

    # Global variables
    USERS_FILENAME = "users.csv"
    URL = "https://fixtur.es/en/formula-1/qualifiers"

    OUTGOING_SMTP_SERVER = "smtp.gmail.com"
    TLS_PORT = 587

    # Instantiate Formula-1 Web Scraper
    scraper = scraper.RaceScraper(URL)

    # Instantiate repository
    repo = repository.PostgreSQLRepo()
    results = repo.get_users()
    users = [repository.User(row[0], row[1], row[2], row[3]) for row in results]

    # Instantiate api clients
    drivers = f1_api.DriversAPI()
    constructors = f1_api.ConstructorsAPI()

    # eg.

    # Construct service by injecting dependencies (contains business logic)
    # service = Service(scraper, repository)

    # Run service
    # server.run()

    # Load date variable
    today_date_str = datetime.strftime(datetime.today(), '%d/%m/%Y')

    # Load data
    race_weekend_dict = scraper.scrape_races()
    current_event = race_weekend_dict.get(today_date_str)
    user_emails = [user.email_address for user in users]

    # Setup check for if data is loaded and email is sent
    email_sent = False
    data_loaded = False

    print(f'Initialised on {today_date_str} at {get_current_time("%H:%M:%S")}')

    while True:
        if get_current_time("%H:%M") == "08:45" and data_loaded is False:

            # Update the dictionary, current event, and the list of user emails
            race_weekend_dict = scraper.scrape_races()
            current_event = race_weekend_dict.get(today_date_str)
            user_emails = [user.email_address for user in users]

            email_sent = False
            data_loaded = True
            print(f'Data loaded on {today_date_str} at {get_current_time("%H:%M:%S")}')

        elif get_current_time("%H:%M") == "09:00" and current_event and email_sent is False:

            send_email(user_emails)
            email_sent = True
            data_loaded = False

        else:
            time.sleep(30)
