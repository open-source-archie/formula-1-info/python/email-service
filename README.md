# Formula1-Email-Bot
This is an email bot to notify recipients of Formula 1 qualifying and race starting times at the start of the weekend. 

### Environment Variables
Required environment variables for main.py.

| ENV_KEY | Description | 
| ------ | ------ |
| SENDER_EMAIL_ADDRESS | Email address of the sending SMTP server |
| SENDER_EMAIL_PASSWORD | Password of the sending SMTP server |


