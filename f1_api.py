import requests
from abc import ABC, abstractmethod
import pandas as pd


class API(ABC):
    @abstractmethod
    def get_standings(self):
        pass

    def standings_to_html(self):
        pass


class DriversAPI(API):
    def __init__(self):
        self.url = 'http://ergast.com/api/f1/current/driverStandings.json'

        response = requests.get(self.url)
        self.data = response.json()

    def get_standings(self):
        return self.data['MRData']['StandingsTable']['StandingsLists'][0]['DriverStandings']

    def standings_to_html(self):
        driver_standings_names = [f'{_driver["Driver"]["givenName"]} {_driver["Driver"]["familyName"]}' for _driver in self.get_standings()]
        driver_standings_points = [_driver['points'] for _driver in self.get_standings()]
        driver_standings_df = pd.DataFrame(list(zip(driver_standings_names, driver_standings_points)),
                                           columns=['Name', 'Points'])
        return driver_standings_df.to_html()


class ConstructorsAPI(API):
    def __init__(self):
        self.url = 'http://ergast.com/api/f1/current/constructorStandings.json'

        response = requests.get(self.url)
        self.data = response.json()

    def get_standings(self):
        return self.data['MRData']['StandingsTable']['StandingsLists'][0]['ConstructorStandings']

    def standings_to_html(self):
        pass
