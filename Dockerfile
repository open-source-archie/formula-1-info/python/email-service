FROM python:3.9.7-slim

WORKDIR /usr/src/app

COPY requirements.txt ./
COPY users.csv ./

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD ["python", "./main.py"]
