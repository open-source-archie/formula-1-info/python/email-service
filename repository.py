from dataclasses import dataclass
import psycopg2
from abc import ABC, abstractmethod
import configparser


class UserRepo(ABC):
    @abstractmethod
    def get_users(self):
        pass

    @abstractmethod
    def get_user(self, user_id: str):
        pass


class PostgreSQLRepo(UserRepo):
    def __init__(self):
        config = configparser.ConfigParser()
        config.read('config.ini')
        db_config = dict(config.items('user_database'))

        self.database = db_config['database']
        self.user = db_config['user']
        self.password = db_config['password']
        self.host = db_config['host']
        self.port = db_config['port']

    def get_users(self):
        conn = psycopg2.connect(database=self.database, user=self.user, password=self.password, host=self.host,
                                port=self.port)
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM users')
        return cursor.fetchall()

    def get_user(self, user_id: str):
        conn = psycopg2.connect(database=self.database, user=self.user, password=self.password, host=self.host,
                                port=self.port)
        cursor = conn.cursor()
        cursor.execute('SELECT  FROM users')
        return cursor.fetchone()


@dataclass
class User:
    user_id: int
    firstname: str
    lastname: str
    email_address: str
