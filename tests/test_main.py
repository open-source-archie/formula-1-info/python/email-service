import unittest
from main import parse_user_emails_from_csv


class TestMainFunctions(unittest.TestCase):

    def test_parse_user_emails_from_csv(self):
        # Define inputs
        input_filename = "test_users.csv"

        # Get result of function being tested
        actual_result = parse_user_emails_from_csv(input_filename)

        # Define expected result
        expected_result = ["jaa.skeoch@gmail.com", "archie.skeoch@gmail.com"]

        self.assertEqual(actual_result.tolist(), expected_result)


if __name__ == '__main__':
    unittest.main()
